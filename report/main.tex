\documentclass[a4paper]{article}

\usepackage[parfill]{parskip}
\usepackage[utf8]{inputenc}
\usepackage[sorting=none]{biblatex}
\usepackage[autostyle, english = american]{csquotes}
\usepackage[british]{babel}
\usepackage{bbm}
\usepackage{bookmark}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{gensymb}
\usepackage{hyperref}
\addbibresource{main.bib}
\emergencystretch=2em

\title{Report: CT Image Reconstruction}
\author{Mihály Csonka}
\date{\today}

\begin{document}

\maketitle

\section{Preface}
This project was made individually. As such, only reconstruction using direct Fourier and filtered backprojection were implemented. Rebinning and sinogram generation were not implemented.

\section{Extras}
Following extras were also implemented:
\begin{enumerate}
    \item Detect whether the sensor images are stacked horizontally or vertically in the sinogram.\\
    This is discussed in Section~\ref{subsubsec:sinogram_rotation}.
    \item Detect whether the sinogram is a 180\degree~or 360\degree~sinogram.\\
    This is discussed in Section~\ref{subsubsec:angular_range}.
    \item Sharpening of the result image.\\
    This is discussed in Section~\ref{subsubsec:sharpening}.
\end{enumerate}
    
\section{Usage}\label{sec:usage}
\textbf{The easiest way to test the application is to run either `try\_CTSlice' or `try\_CTRadon' from within Matlab}.\\
\\
There are 2 files which serve as entry-points to the application, namely `try\_CTSlice.m' and `try\_CTRadon.m'. These are for direct Fourier transform and backprojection respectively. Both files can take following \textit{optional}, but fixed-position, parameters:
\begin{itemize}
    \item file\_path: the path to the sinogram image file (Default: `Walnut.png').
    \item padding: amount of units used to zero-pad each side of a sample with (Default: ensures sample resolution to be at least 3000).
    \item sharpen\_cutoff: Butterworth low-pass filter cutoff (Default: 100).
    \item sharpen\_factor: Butterworth low-pass filter factor (Default: 1.0).
\end{itemize}
Thus, to test e.g.~backprojection on a custom sinogram one would run:\\ `try\_CTRadon /path/to/sinogram.img\_ext'.

\section{Implementation}
This section discusses what each file in the `src' directory implements. The files `main.m', `try\_CTSlice.m', and `try\_CTRadon.m' are excluded as these have already been discussed in Section~\ref{sec:usage}. Files are mentioned in order of usage.

\subsection{Pre-processing}
Following function/files are used to prepare input sinograms and generate parameters which are then to be used during the reconstruction.

\subsubsection{Consistent sinogram rotation}\label{subsubsec:sinogram_rotation}
The file `fix\_sinogram\_rotation.m' detects the rotation of the input sinogram and rotates it if necessary. Functions further in the pipeline expect each sample to be stored as a row.\\
It is assumed that no sinograms are made of perfectly absorbing material. Then, the only way to have every value of a row/column be 0 is when that row/column consists of the outer edges of samples. I.e.~if a row is completely 0 then sensor samples are stacked horizontally, and vice versa.

\subsubsection{Zero padding}\label{subsubsec:zero_pad}
The file `zero\_pad\_sinogram.m' pads the edges of each sample of a sinogram with a given amount of 0-values. This increases the resolution of a sample. I.e.~it increases the amount of frequencies, and thus the precision, of the resulting Fourier transform.\\
A huge quality improvement, at the cost of some processing time, was observed after the implementation of this technique (see Figure~\ref{figure:padding_comparison}).

\begin{figure}[!h]
    \centering
    \includegraphics[width=\linewidth]{img/padding_comparison.png}
    \caption{Comparison between running the \textit{CTSlice} function on the sinogram in `Walnut.png' (left) and the padded version thereof (right). 1000 units of padding were added per side of a sample.}\label{figure:padding_comparison}
\end{figure}

\subsubsection{Angular range of a sinogram}\label{subsubsec:angular_range}
The file `determine\_angular.m' determines the degree, either 180\degree~or 360\degree, of a sinogram.\\
Initially, an attempt was made to detect the degree by means of comparing the first and last sample of the sinogram. The idea was that there would be no significant difference between these samples for 360\degree~sinograms as the sensor would have performed a full rotation. On the other hand, 180\degree~sinograms would show a significant difference. This approach failed in practice. The differences between samples did not differ significantly enough between 180\degree~and 360\degree~sinograms, such that the angular range of sinograms could not be determined.\\
A naive approach, but one that does work for all the provided example data, was then implemented. If there are at least 360 samples then a 360\degree~sinogram is assumed. Otherwise, 180\degree~is assumed. This approach is actually incorrect, e.g.~a 180\degree~scan could have samples taken every 0.5\degree~and thus result in 360 samples.

\subsection{Image reconstruction}
Following functions/files implement the actual sinogram to image reconstruction.

\subsubsection{CTSlice}
The file `CTSlice.m' implements CT image reconstruction using the direct Fourier transform approach. The implementation is based on that described in \textit{Fourier Methods in CT}, which was supplied by the teaching staff~\cite{direct_fourier}.\\
The implementation consists of only 3 steps:
\begin{enumerate}
    \item Apply the Fast Fourier Transform (FFT) per sample in the sinogram.
    \item Map each transformed sample to its location in the Fourier amplitude spectrum.
    \item Apply the Inverse Fast Fourier Transform (IFFT) on the whole amplitude spectrum.
\end{enumerate}
Initially the implementation performed step 2 explicitly. I.e.~each transformed sample was rotated and added to a blank canvas which was to be the resulting amplitude spectrum. It was then interpolated to ``dilute'' the values over the coordinate system.\\
This approach was slow, consumed a lot of memory, and produced poor results. Re-reading the aforementioned paper cleared up some confusions. Consequently, step 2 was rewritten to take full advantage of Matlab's \textit{interp2} function.\\
Each sample in a sinogram is recorded at an angle with a certain degree. I.e.~a sinogram's samples are stored with a polar coordinate system. The goal is to map them to the amplitude spectrum, which uses a cartesian coordinate system. To achieve this, a \textit{meshgrid} containing cartesian coordinates of the desired resulting image size was generated. The cartesian coordinates were then converted to polar coordinates using \textit{cart2pol}, which were then used to sample the sinogram using \textit{interp2}.

\subsubsection{CTRadon}
The file `CTRadon.m' implements CT image reconstruction using the backprojection method. The implementation follows the explanation given by the teaching staff and as described in the provided paper~\cite{backprojection}.\\
As suggested by the teaching staff, the \textit{iradon} function was used to ``smear'' out samples and rotate them according to the degree they were recorded at. Smeared out samples were added one by one onto the result image canvas and eventually averaged out over the amount of samples.\\
As already hinted by the teaching staff, the resulting image ended up being blurry and could be described as overexposed. The lower frequencies, which represent the average value of pixels, were too high. On the other hand, the higher frequencies, which represent the details, were too low. This was easily solved by transforming each sample with the FFT, applying a RAM-LAK filter to each of them, and restoring the samples with the IFFT. This was done before the previously explained step of smearing out samples. A RAM-LAK filter is a simple symmetrical high-pass filter which linearly goes from 1 to 0, and linearly back to 1.

\subsection{Post-processing}
Following functions/files perform post-processing steps on the resulting reconstructed image.

\subsubsection{Sharpening}\label{subsubsec:sharpening}
The file `sharpen.m' is used to sharpen an image. It is a simple implementation which adds details, acquired from the difference between the original and blurred version of the image, to the original image.\\
A Butterworth low-pass filter, implemented in the file `butterworth\_lowpass.m', is used to blur images.\\
Sharpening improves the quality of image reconstructions noticeably, as can be observed in Figure~\ref{figure:sharpen_comparison}.

\begin{figure}[!h]
    \centering
    \includegraphics[width=\linewidth]{img/sharpen_comparison.png}
    \caption{Comparison between the result of running the \textit{CTSlice} function on `Walnut.png' (left) and the sharpened version thereof (right). Used Butterworth low-pass parameters are \(cutoff=100\) and \(factor=1\).}\label{figure:sharpen_comparison}
\end{figure}

\subsubsection{Cropping the image}
The file `crop\_image.m' crops an image to the desired dimensions.\\
As explained in Section~\ref{subsubsec:zero_pad}, zero-padding is applied to the sinograms to increase the width/resolution of samples. As a result the reconstructed images also have a high resolution. However, the size of the reconstructed image's content is not increased. Thus, the resulting image is cropped back to a square with the width being the original sample resolution. This square contains the actual reconstructed content.

\printbibliography

\end{document}
  