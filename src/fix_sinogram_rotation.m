function [sinogram] = fix_sinogram_rotation(sinogram)
    % Input:
    % - greyscale sinogram (2D matrix)
    % Output:
    % - greyscale sinogram (2D matrix). Potentially rotated such that each row is an angle.

    % we expect a slice/rotation to be stored as a row.
    % So if there is no data in a row, the image probably needs rotation.
    needs_rotation = max(sinogram(1, :)) == 0;

    if needs_rotation
        sinogram = rot90(sinogram);
    end
