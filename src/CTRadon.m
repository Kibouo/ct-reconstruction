function [reconstructed] = CTRadon(sinogram, angular)
    % Input:
    % - greyscale sinogram (2D matrix),
    %       with normalised pixel values (values in range [0,1]),
    %       and each row being a measurement from an angle.
    % - angular range of the sinogram
    % Output:
    % - reconstructed greyscale image (2D matrix)
    [amt_samples, width] = size(sinogram);
    reconstructed = zeros(width, width);

    fft_sinogram = fftshift(fft(ifftshift(sinogram), [], 2));

    % apply RAM-LAK filter per row
    ram_lak_filter = abs(linspace(-1, 1, width));
    ram_lak_filter = repmat(ram_lak_filter, [amt_samples 1]);
    fft_sinogram = fft_sinogram .* ram_lak_filter;

    sinogram = fftshift(ifft(ifftshift(fft_sinogram), [], 2));

    % theta = rotation angle = nth row of sinogram
    for sample_idx = 1:amt_samples
        theta = sample_idx / amt_samples * angular;
        projection = real(rot90(sinogram(sample_idx, :), 3));
        smeared = iradon([projection projection], [theta theta], "linear", "none", 1, width);

        reconstructed = reconstructed + smeared;
    end

    reconstructed = reconstructed / amt_samples;
