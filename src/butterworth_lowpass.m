function [result] = butterworth_lowpass(grey_img, cutoff, factor)
    % Applies butterworth lowpass filter to the fourier amplitude spectrum of
    % the provided greyscale image. Image is supplied as matrix. Returns resulting image.
    [img_height, img_width, ~] = size(grey_img);

    u = 0:(img_height - 1);
    v = 0:(img_width - 1);
    idx = find(u > img_height / 2);
    u(idx) = u(idx) - img_height;
    idy = find(v > img_width / 2);
    v(idy) = v(idy) - img_width;

    [V, U] = meshgrid(v, u);
    distances = sqrt(U.^2 + V.^2);

    filter = 1 ./ (1 + (distances ./ cutoff).^(2 * factor));

    result = real(ifft2(double(filter .* fft2(grey_img))));
