function [reconstructed] = CTSlice(sinogram, angular)
    % Input:
    % - greyscale sinogram (2D matrix),
    %       with normalised pixel values (values in range [0,1]),
    %       and each row being a measurement from an angle.
    % - angular range of the sinogram
    % Output:
    % - reconstructed greyscale image (2D matrix)
    fft_sinogram = fftshift(fft(ifftshift(sinogram), [], 2));
    [amt_samples, diameter] = size(sinogram);

    [X_query, Y_query] = meshgrid((1:diameter) - floor(diameter / 2));
    [theta_query, rho_query] = cart2pol(X_query, Y_query);
    theta_query = rad2deg(theta_query); % cart2pol returns radians
    % degrees > 180 are represented by rotating the other way around (negative ones up till -180).
    % Of course, negative coordinates are nonsense so we fix this...
    theta_query = theta_query + 180;

    [rho, theta] = meshgrid((1:diameter) - floor(diameter / 2), (1:amt_samples) / amt_samples * angular);
    fft_circle = interp2(rho, theta, fft_sinogram, rho_query, theta_query, "cubic");
    fft_circle(isnan(fft_circle)) = 0;

    % our sampling results in mirrored images. The algorithm should still be correct, it's just annoying.
    fft_circle = fliplr(fft_circle);
    reconstructed = real(fftshift(ifft2(ifftshift(fft_circle))));
