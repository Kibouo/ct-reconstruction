function [cropped] = crop_image(image, desired_height, desired_width)
    % Input:
    % - greyscale image (2D matrix)
    % - desired height/width of the image
    % Output:
    % - cropped image. This will be the middle `desired * desired` pixels of the input
    [height, width] = size(image);
    start_height = floor((height / 2) - (desired_height / 2));
    start_width = floor((width / 2) - (desired_width / 2));

    cropped = image(start_height + 1:start_height + desired_height, start_width + 1:start_width + desired_width);
