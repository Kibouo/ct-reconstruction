function [] = main(file_path, method, padding, sharpen_cutoff, sharpen_factor, angular)

    if nargin < 1
        file_path = "../data/parallel/Walnut.png";
    end

    sinogram = im2gray(cast(imread(file_path), "double"));
    sinogram = fix_sinogram_rotation(sinogram);
    width = size(sinogram, 2);

    if nargin < 3
        padding = max((3000 - width) / 2, 0);
    end

    sinogram = zero_pad_sinogram(sinogram, padding);
    
    if nargin < 6
        angular = determine_angular(sinogram);
    end
        
    if nargin < 2
        method = 0;
    end

    if method == 0
        result = CTRadon(sinogram, angular);
    else
        result = CTSlice(sinogram, angular);
    end

    if nargin < 4
        sharpen_cutoff = 100;
    end

    if nargin < 5
        sharpen_factor = 1.0;
    end

    result = sharpen(result, sharpen_cutoff, sharpen_factor);
    result = crop_image(result, width, width);

    imshow(result);
