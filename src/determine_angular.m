function [angular] = determine_angular(sinogram)
    % Input:
    % - greyscale sinogram (2D matrix) with each row a sample
    % Output:
    % - angular (180 or 360)

    height = size(sinogram, 1);

    if height >= 360
        angular = 360;
    else
        angular = 180;
    end
