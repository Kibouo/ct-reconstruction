function [sinogram] = zero_pad_sinogram(sinogram, padding)
    % Input:
    % - greyscale sinogram (2D matrix) with each row a sample
    % - amount of units to pad with, both left and right
    % Output:
    % - zero-padded sinogram (2D matrix) with each row a sample

    % NOTE: gives insane quality improvement.
    % TODO: tweak padding amount (speed calcs vs. quality)
    sinogram = padarray(sinogram, [0 padding], 0, "both");
