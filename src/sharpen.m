function [sharpened] = sharpen(grey_img, butterworth_cutoff, butterworth_factor)
    % Input:
    % - greyscale image
    % - cutoff for butterworth lowpass filter
    % - factor for butterworth lowpass filter
    % Output:
    % - sharpened version of greyscale image.
    blurred = butterworth_lowpass(grey_img, butterworth_cutoff, butterworth_factor);
    difference = grey_img - blurred;
    sharpened = grey_img + difference;
