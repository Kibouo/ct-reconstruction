function [] = try_CTSlice(file_path, padding, sharpen_cutoff, sharpen_factor, angular)

    if nargin < 1
        main("../data/parallel/Walnut.png", 1);
    elseif nargin < 2
        main(file_path, 1);
    elseif nargin < 3
        main(file_path, 1, str2num(padding));
    elseif nargin < 4
        main(file_path, 1, str2num(padding), str2num(sharpen_cutoff));
    elseif nargin < 5
        main(file_path, 1, str2num(padding), str2num(sharpen_cutoff), str2num(sharpen_factor));
    elseif nargin < 6
        main(file_path, 1, str2num(padding), str2num(sharpen_cutoff), str2num(sharpen_factor), str2num(angular));
    end
